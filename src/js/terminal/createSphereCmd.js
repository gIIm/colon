class CreateSphereCmd {

    constructor(terminalDomElement, logger) {

        var _this = this;

        // PARAMS
        this.params = {
            mode: "face",
            num: 1,
            radius: 100,
            resolution: 24,
            posX: Math.random() * 600 - 300,
            posY: Math.random() * 600 - 300,
            posZ: Math.random() * -500,
            rotX: Math.random() * Math.PI * 2,
            rotY: Math.random() * Math.PI * 2,
            rotZ: Math.random() * Math.PI * 2,
            color: "#fff"
        };

        this.sphereNumMax = 1000;
        this.sphereRotMin = 0.0;
        this.sphereRotMax = 0.15;

        // DOM ELEMENT
        this.terminalDomElement = terminalDomElement;

        // LOGGER
        this.logger = logger;

        this.commandName = "create";
        this.artefactName = "sphere";
        this.artefactIndex = 2;

        // EVENT
        this.createSphereEvent = new CustomEvent('createSphere');

    }

    listen(command, term) {

        var _this = this;

        if (command == 'create:') {

            var history = term.history();

            history.disable();


            term.push(function(command) {

                    if (command.match(/^(re)$/i)) {

                        term.pop();

                        history.enable();
                        return;
                    }


                    //// SPHERE ////
                    if (command.match(/^(sphere)$/i)) {

                        term.echo("::SPHERE::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorConfirm);
                            }
                        });


                        _this.logger.createNewTyper();

                        if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                        _this.logger.typer.line();
                        if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                        _this.logger.typer.line();
                        if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));

                        // SPHERE MODE
                        term.push(function(command) {

                                if (command.match(/^(re)$/i)) {

                                    term.pop();

                                    history.enable();
                                    return;
                                }

                                if (command.match(/^(point|points|wireframe|face|faces)$/i)) {

                                    var mode = command;

                                    _this.params.mode = mode;

                                    term.echo("::SPHERE MODE::" + mode, {
                                        finalize: function(div) {
                                            div.css("background-color", _this.logger.typerColorConfirm);
                                        }
                                    });


                                    _this.logger.createNewTyper();

                                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                                    _this.logger.typer.line();
                                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                                    _this.logger.typer.line();
                                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));




                                    // SPHERE NUM
                                    term.push(function(command) {

                                            if (command.match(/^(re)$/i)) {

                                                term.pop();

                                                history.enable();
                                                return;
                                            }


                                            if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                var num = Math.abs(command);
                                                if (num > _this.sphereNumMax) num = _this.sphereNumMax;

                                                _this.params.num = num;

                                                term.echo("::SPHERE NUM:: " + num, {
                                                    finalize: function(div) {
                                                        div.css("background-color", _this.logger.typerColorConfirm);
                                                    }
                                                });

                                                _this.logger.createNewTyper();

                                                _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE NUM:: " + num + "</span>")

                                                if (num > 1) {

                                                    // Dispatch the event.
                                                    _this.createSphereEvent.params = _this.params;
                                                    _this.terminalDomElement.dispatchEvent(_this.createSphereEvent);

                                                    term.pop();
                                                    term.pop();
                                                    term.pop();
                                                    term.pop();

                                                    history.enable();

                                                    return;
                                                }




                                                // SPHERE RADIUS
                                                term.push(function(command) {

                                                       
                                             

                                                             if (command.match(/^(re)$/i)) {

                                                            term.pop();

                                                            history.enable();
                                                            return;
                                                        }

                                                        var radius = Math.abs(command);

                                                        _this.params.radius = radius;

                                                        term.echo("::SPHERE RADIUS:: " + radius, {
                                                            finalize: function(div) {
                                                                div.css("background-color", _this.logger.typerColorConfirm);
                                                            }
                                                        });

                                                        _this.logger.createNewTyper();

                                                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE RADIUS:: " + radius + "</span>")






                                                            // SPHERE RESOLUTION
                                                            term.push(function(command) {

                                                                    if (command.match(/^(re)$/i)) {

                                                                        term.pop();

                                                                        history.enable();
                                                                        return;
                                                                    }

                                                                    if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                        var resolution = Math.abs(command);

                                                                        _this.params.resolution = resolution;

                                                                        term.echo("::SPHERE RESOLUTION:: " + resolution, {
                                                                            finalize: function(div) {
                                                                                div.css("background-color", _this.logger.typerColorConfirm);
                                                                            }
                                                                        });

                                                                        _this.logger.createNewTyper();

                                                                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE RESOLUTION:: " + resolution + "</span>")






                                                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {



                                                                            // SPHERE POS X
                                                                            term.push(function(command) {

                                                                                if (command.match(/^(re)$/i)) {

                                                                                    term.pop();

                                                                                    history.enable();
                                                                                    return;
                                                                                }


                                                                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                                    var posX = command;

                                                                                    _this.params.posX = posX;

                                                                                    term.echo("::SPHERE POS X:: " + posX, {
                                                                                        finalize: function(div) {
                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                        }
                                                                                    });

                                                                                    _this.logger.createNewTyper();

                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE POS X:: " + posX + "</span>")




                                                                                    // SPHERE POS Y
                                                                                    term.push(function(command) {

                                                                                        if (command.match(/^(re)$/i)) {

                                                                                            term.pop();

                                                                                            history.enable();
                                                                                            return;
                                                                                        }


                                                                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                                            var posY = command;

                                                                                            _this.params.posY = posY;

                                                                                            term.echo("::SPHERE POS Y:: " + posY, {
                                                                                                finalize: function(div) {
                                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                }
                                                                                            });

                                                                                            _this.logger.createNewTyper();

                                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE POS Y:: " + posY + "</span>")




                                                                                            // SPHERE POS Z
                                                                                            term.push(function(command) {

                                                                                                if (command.match(/^(re)$/i)) {

                                                                                                    term.pop();

                                                                                                    history.enable();
                                                                                                    return;
                                                                                                }


                                                                                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                                                    var posZ = command;

                                                                                                    _this.params.posZ = posZ;

                                                                                                    term.echo("::SPHERE POS Z:: " + posZ, {
                                                                                                        finalize: function(div) {
                                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                        }
                                                                                                    });

                                                                                                    _this.logger.createNewTyper();

                                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE POS Z:: " + posZ + "</span>")




                                                                                                    // SPHERE ROT X
                                                                                                    term.push(function(command) {

                                                                                                        if (command.match(/^(re)$/i)) {

                                                                                                            term.pop();

                                                                                                            history.enable();
                                                                                                            return;
                                                                                                        }

                                                                                                        if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                                            var rotX = Number(command);
                                                                                                            rotX = rotX.map(0, 10, _this.sphereRotMin, _this.sphereRotMax);

                                                                                                            _this.params.rotX = rotX;

                                                                                                            term.echo("::SPHERE ROT X:: " + rotX, {
                                                                                                                finalize: function(div) {
                                                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                }
                                                                                                            });

                                                                                                            _this.logger.createNewTyper();

                                                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE ROT X:: " + rotX + "</span>");



                                                                                                            // SPHERE ROT Y
                                                                                                            term.push(function(command) {

                                                                                                                if (command.match(/^(re)$/i)) {

                                                                                                                    term.pop();

                                                                                                                    history.enable();
                                                                                                                    return;
                                                                                                                }

                                                                                                                if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                                                    var rotY = Number(command);
                                                                                                                    rotY = rotY.map(0, 10, _this.sphereRotMin, _this.sphereRotMax);

                                                                                                                    _this.params.rotY = rotY;

                                                                                                                    term.echo("::SPHERE ROT Y:: " + rotY, {
                                                                                                                        finalize: function(div) {
                                                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                        }
                                                                                                                    });

                                                                                                                    _this.logger.createNewTyper();

                                                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE ROT Y:: " + rotY + "</span>");




                                                                                                                    // SPHERE ROT Z
                                                                                                                    term.push(function(command) {

                                                                                                                        if (command.match(/^(re)$/i)) {

                                                                                                                            term.pop();

                                                                                                                            history.enable();
                                                                                                                            return;
                                                                                                                        }

                                                                                                                        if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                                                            var rotZ = Number(command);
                                                                                                                            rotZ = rotZ.map(0, 10, _this.sphereRotMin, _this.sphereRotMax);

                                                                                                                            _this.params.rotZ = rotZ;

                                                                                                                            term.echo("::SPHERE ROT Z:: " + rotZ, {
                                                                                                                                finalize: function(div) {
                                                                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                                }
                                                                                                                            });

                                                                                                                            _this.logger.createNewTyper();

                                                                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::SPHERE ROT Z:: " + rotZ + "</span>")




                                                                                                                            // COLOR
                                                                                                                            term.push(function(command) {

                                                                                                                                if (command.match(/^(re)$/i)) {

                                                                                                                                    term.pop();

                                                                                                                                    history.enable();
                                                                                                                                    return;
                                                                                                                                }

                                                                                                                                if (command.match(/^[a-zA-Z0-9_#(),.-]*$/)) {

                                                                                                                                    var color = command;

                                                                                                                                    _this.params.color = color;

                                                                                                                                    term.echo("::SPHERE COLOR:: " + color, {
                                                                                                                                        finalize: function(div) {
                                                                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                                        }
                                                                                                                                    });

                                                                                                                                    _this.logger.createNewTyper();

                                                                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::COLOR:: " + color + "</span>")



                                                                                                                                    _this.createSphereEvent.params = _this.params;
                                                                                                                                    _this.terminalDomElement.dispatchEvent(_this.createSphereEvent);

                                                                                                                                                                                                                                                                        term.pop();
                        
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();

                                                                                                                                    history.enable();

                                                                                                                                    return;




                                                                                                                                }
                                                                                                                            }, {
                                                                                                                                prompt: '::color: '
                                                                                                                            });




                                                                                                                        }
                                                                                                                    }, {
                                                                                                                        prompt: '::rot::z: '
                                                                                                                    });




                                                                                                                }
                                                                                                            }, {
                                                                                                                prompt: '::rot::y: '
                                                                                                            });




                                                                                                        }
                                                                                                    }, {
                                                                                                        prompt: '::rot::x: '
                                                                                                    });




                                                                                                }
                                                                                            }, {
                                                                                                prompt: '::pos::z: '
                                                                                            });




                                                                                        }
                                                                                    }, {
                                                                                        prompt: '::pos::y: '
                                                                                    });




                                                                                }
                                                                            }, {
                                                                                prompt: '::pos::x: '
                                                                            });




                                                                        }














}

                                                                    }, {
                                                                        prompt: '::resolution: '
                                                                    });



                                                        }, {
                                                            prompt: '::radius: '
                                                        });



                                                }
                                            }, {
                                                prompt: '::num: '
                                            });


                                    }
                                }, {
                                    prompt: '::mode: '
                                });

                        }

                    }, {
                        prompt: '::create: '
                    });


            }

            (function() {
                Math.clamp = function(a, b, c) {
                    return Math.max(b, Math.min(c, a));
                }
            })();

            Number.prototype.map = function(in_min, in_max, out_min, out_max) {
                var val = (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
                if (val < out_min) val = out_min;
                if (val > out_max) val = out_max;
                return val;
            }

        }

    }
    export default CreateSphereCmd;