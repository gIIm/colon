import Commands from './commands';
import Typer from 'typer-js/typer.min.js';

class Logger {

    constructor(element) {

        var _this = this;

        // COMMANDS DATA
        this.commands = Commands;

        // DOM ELEMENT
        this.domElement = element;
        
        // TYPER
        // this.typer = new Typer(this.domElement, 10);
        this.typerOpts = {block: true, blink: 'hard', speed: 10};
        this.typerColorConfirm = "#3d3d3d";
        this.typerColorError = "#f20000";

        this.isInspect = false;

    }

    createNewTyper() {

      if (this.typer) this.typer.kill();
      this.typer = new Typer(this.domElement, this.typerOpts);

      this.typer.cursor(this.typerOpts);

      this.typer.empty();

    }   

    listen(command) {

      var _this = this;

      if (command == 'release') {
          
          this.isInspect = true;

      }

      if (command == 'follow') {
          
          this.isInspect = false;

      }

      if (command == 'signal') {
          
          this.log(command);

      }

      if (command == 'debug') {
          
          this.log(command);

      }

      if (command == 'ascii') {
          
          this.log(command);

      }

      if (command == 'flush') {

          this.typer.empty();

          var audioLoggerElement = document.querySelector('#audio-logger-data');
          audioLoggerElement.textContent = " ";

          this.log(command);

      }

      if (command == 'create') {

          this.logIndexed(command, 0);

      }

      if (command == 'delete') {

          this.logIndexed(command, 0);

      }

      if (command == 'goto') {

          this.logIndexed(command, 0);

      }

    }

    log(commandName) {
      
      this.createNewTyper();
      
      if(this.commands[commandName].command) this.typer.line(JSON.stringify(this.commands[commandName].command)); 
      this.typer.line();
      if(this.commands[commandName].parameters) this.typer.line("parameters: <br>" + JSON.stringify(this.commands[commandName].parameters)); 
      this.typer.line();
      if(this.commands[commandName].info) this.typer.line("info: <br>" + JSON.stringify(this.commands[commandName].info)); 

    }

    logIndexed(commandName, index = 0) {
      
      this.createNewTyper();
      
      if(this.commands[commandName][index].command) this.typer.line(JSON.stringify(this.commands[commandName][index].command)); 
      this.typer.line();
      if(this.commands[commandName][index].parameters) this.typer.line("parameters: <br>" + JSON.stringify(this.commands[commandName][index].parameters)); 
      this.typer.line();
      if(this.commands[commandName][index].info) this.typer.line("info: <br>" + JSON.stringify(this.commands[commandName][index].info)); 

    }

}
export default Logger;