class CreateBoxCmd {

    constructor(terminalDomElement, logger) {

        var _this = this;

        // PARAMS
        this.params = {
            mode: "face",
            num: 1,
            size: 100,
            posX: Math.random() * 600 - 300,
            posY: Math.random() * 600 - 300,
            posZ: Math.random() * -500,
            rotX: Math.random() * Math.PI * 2,
            rotY: Math.random() * Math.PI * 2,
            rotZ: Math.random() * Math.PI * 2,
            color: "#fff"
        };

        this.boxNumMax = 1000;
        this.boxRotMin = 0.0;
        this.boxRotMax = 0.15;

        // DOM ELEMENT
        this.terminalDomElement = terminalDomElement;

        // LOGGER
        this.logger = logger;

        this.commandName = "create";
        this.artefactName = "box";
        this.artefactIndex = 1;

        // EVENT
        this.createBoxEvent = new CustomEvent('createBox');

    }

    listen(command, term) {

        var _this = this;

        if (command == 'create') {

            var history = term.history();

            history.disable();


            term.push(function(command) {

                if (command.match(/^(re)$/i)) {

                    term.pop();

                    history.enable();
                    return;
                }

                //// BOX ////
                if (command.match(/^(box)$/i)) {
                    term.echo("::BOX::", {
                        finalize: function(div) {
                            div.css("background-color", _this.logger.typerColorConfirm);
                        }
                    });


                    _this.logger.createNewTyper();

                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));

                    // BOX MODE
                    term.push(function(command) {

                        if (command.match(/^(re)$/i)) {

                            term.pop();

                            history.enable();
                            return;
                        }

                        if (command.match(/^(point|points|wireframe|face|faces)$/i)) {

                            var mode = command;

                            _this.params.mode = mode;

                            term.echo("::BOX MODE::" + mode, {
                                finalize: function(div) {
                                    div.css("background-color", _this.logger.typerColorConfirm);
                                }
                            });


                            _this.logger.createNewTyper();

                            if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                            _this.logger.typer.line();
                            if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                            _this.logger.typer.line();
                            if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));




                            // BOX NUM
                            term.push(function(command) {

                                if (command.match(/^(re)$/i)) {

                                    term.pop();

                                    history.enable();
                                    return;
                                }


                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                    var num = Math.abs(command);
                                    if (num > _this.boxNumMax) num = _this.boxNumMax;

                                    _this.params.num = num;

                                    term.echo("::BOX NUM:: " + num, {
                                        finalize: function(div) {
                                            div.css("background-color", _this.logger.typerColorConfirm);
                                        }
                                    });

                                    _this.logger.createNewTyper();

                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX NUM:: " + num + "</span>")

                                    if (num > 1) {

                                        // Dispatch the event.
                                        _this.createBoxEvent.params = _this.params;
                                        _this.terminalDomElement.dispatchEvent(_this.createBoxEvent);

                                        term.pop();
                                        term.pop();
                                        term.pop();

                                        history.enable();

                                        return;
                                    }




                                    // BOX SIZE
                                    term.push(function(command) {

                                        if (command.match(/^(re)$/i)) {

                                            term.pop();

                                            history.enable();
                                            return;
                                        }

                                        var size = Math.abs(command);

                                        _this.params.size = size;

                                        term.echo("::BOX SIZE:: " + size, {
                                            finalize: function(div) {
                                                div.css("background-color", _this.logger.typerColorConfirm);
                                            }
                                        });

                                        _this.logger.createNewTyper();

                                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX SIZE:: " + size + "</span>")


                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {



                                            // BOX POS X
                                            term.push(function(command) {

                                                if (command.match(/^(re)$/i)) {

                                                    term.pop();

                                                    history.enable();
                                                    return;
                                                }


                                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                    var posX = command;

                                                    _this.params.posX = posX;

                                                    term.echo("::BOX POS X:: " + posX, {
                                                        finalize: function(div) {
                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                        }
                                                    });

                                                    _this.logger.createNewTyper();

                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX POS X:: " + posX + "</span>")




                                                    // BOX POS Y
                                                    term.push(function(command) {

                                                        if (command.match(/^(re)$/i)) {

                                                            term.pop();

                                                            history.enable();
                                                            return;
                                                        }


                                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                            var posY = command;

                                                            _this.params.posY = posY;

                                                            term.echo("::BOX POS Y:: " + posY, {
                                                                finalize: function(div) {
                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                }
                                                            });

                                                            _this.logger.createNewTyper();

                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX POS Y:: " + posY + "</span>")




                                                            // BOX POS Z
                                                            term.push(function(command) {

                                                                if (command.match(/^(re)$/i)) {

                                                                    term.pop();

                                                                    history.enable();
                                                                    return;
                                                                }


                                                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                    var posZ = command;

                                                                    _this.params.posZ = posZ;

                                                                    term.echo("::BOX POS Z:: " + posZ, {
                                                                        finalize: function(div) {
                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                        }
                                                                    });

                                                                    _this.logger.createNewTyper();

                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX POS Z:: " + posZ + "</span>")




                                                                    // BOX ROT X
                                                                    term.push(function(command) {

                                                                        if (command.match(/^(re)$/i)) {

                                                                            term.pop();

                                                                            history.enable();
                                                                            return;
                                                                        }

                                                                        if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                            var rotX = Number(command);
                                                                            rotX = rotX.map(0, 10, _this.boxRotMin, _this.boxRotMax);

                                                                            _this.params.rotX = rotX;

                                                                            term.echo("::BOX ROT X:: " + rotX, {
                                                                                finalize: function(div) {
                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                }
                                                                            });

                                                                            _this.logger.createNewTyper();

                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX ROT X:: " + rotX + "</span>")



                                                                            // BOX ROT Y
                                                                            term.push(function(command) {

                                                                                if (command.match(/^(re)$/i)) {

                                                                                    term.pop();

                                                                                    history.enable();
                                                                                    return;
                                                                                }

                                                                                if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                    var rotY = Number(command);
                                                                                    rotY = rotY.map(0, 10, _this.boxRotMin, _this.boxRotMax);

                                                                                    _this.params.rotY = rotY;

                                                                                    term.echo("::BOX ROT Y:: " + rotY, {
                                                                                        finalize: function(div) {
                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                        }
                                                                                    });

                                                                                    _this.logger.createNewTyper();

                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX ROT Y:: " + rotY + "</span>")




                                                                                    // BOX ROT Z
                                                                                    term.push(function(command) {

                                                                                        if (command.match(/^(re)$/i)) {

                                                                                            term.pop();

                                                                                            history.enable();
                                                                                            return;
                                                                                        }

                                                                                        if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                            var rotZ = Number(command);
                                                                                            rotZ = rotZ.map(0, 10, _this.boxRotMin, _this.boxRotMax);

                                                                                            _this.params.rotZ = rotZ;

                                                                                            term.echo("::BOX ROT Z:: " + rotZ, {
                                                                                                finalize: function(div) {
                                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                }
                                                                                            });

                                                                                            _this.logger.createNewTyper();

                                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX ROT Z:: " + rotZ + "</span>")




                                                                                            // COLOR
                                                                                            term.push(function(command) {

                                                                                                if (command.match(/^(re)$/i)) {

                                                                                                    term.pop();

                                                                                                    history.enable();
                                                                                                    return;
                                                                                                }

                                                                                                if (command.match(/^[a-zA-Z0-9_#(),.-]*$/)) {

                                                                                                    var color = command;

                                                                                                    _this.params.color = color;

                                                                                                    term.echo("::BOX COLOR:: " + color, {
                                                                                                        finalize: function(div) {
                                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                        }
                                                                                                    });

                                                                                                    _this.logger.createNewTyper();

                                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::COLOR:: " + color + "</span>")



                                                                                                    _this.createBoxEvent.params = _this.params;
                                                                                                    _this.terminalDomElement.dispatchEvent(_this.createBoxEvent);

                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();
                                                                                                    term.pop();

                                                                                                    history.enable();

                                                                                                    return;




                                                                                                }
                                                                                            }, {
                                                                                                prompt: '::color: '
                                                                                            });




                                                                                        }
                                                                                    }, {
                                                                                        prompt: '::rot::z: '
                                                                                    });




                                                                                }
                                                                            }, {
                                                                                prompt: '::rot::y: '
                                                                            });




                                                                        }
                                                                    }, {
                                                                        prompt: '::rot::x: '
                                                                    });




                                                                }
                                                            }, {
                                                                prompt: '::pos::z: '
                                                            });




                                                        }
                                                    }, {
                                                        prompt: '::pos::y: '
                                                    });




                                                }
                                            }, {
                                                prompt: '::pos::x: '
                                            });



                                        }

                                    }, {
                                        prompt: '::size: '
                                    });



                                }
                            }, {
                                prompt: '::num: '
                            });


                        }
                    }, {
                        prompt: '::mode: '
                    });

                }

            }, {
                prompt: '::create: '
            });


        }

        (function() {
            Math.clamp = function(a, b, c) {
                return Math.max(b, Math.min(c, a));
            }
        })();

        Number.prototype.map = function(in_min, in_max, out_min, out_max) {
            var val = (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
            if (val < out_min) val = out_min;
            if (val > out_max) val = out_max;
            return val;
        }

    }

}
export default CreateBoxCmd;