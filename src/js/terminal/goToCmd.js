import 'three'
import TWEEN from 'tween.js'

class goToCmd {

    constructor(terminalDomElement, logger) {

        var _this = this;

        // PARAMS
        this.params = {
            type: "box",
            id: 0,
            pos: new THREE.Vector3(0, 0, 0),
            speed: 2000
        };

        // DOM ELEMENT
        this.terminalDomElement = terminalDomElement;

        // LOGGER
        this.logger = logger;

        this.commandName = "goto";
        this.artefactName = "artefact";
        this.artefactIndex = 1;
        this.positionName = "pos";
        this.positionIndex = 2;

        // EVENT
        this.goToEvent = new CustomEvent('goto');

    }

    listen(command, term) {

        var _this = this;

        if (command == 'goto') {

            var history = term.history();

            history.disable();


            term.push(function(command) {

                if (command.match(/^(re)$/i)) {

                    term.pop();

                    history.enable();
                    return;
                }

                //// GOTO POS ////
                if (command.match(/^(pos)$/i)) {

                    var type = command;

                    _this.params.type = type;


                    term.echo("::GOTO POS::", {
                        finalize: function(div) {
                            div.css("background-color", _this.logger.typerColorConfirm);
                        }
                    });


                    _this.logger.createNewTyper();

                    if (_this.logger.commands[_this.commandName][_this.positionIndex][_this.positionName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.positionIndex][_this.positionName].command));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.positionIndex][_this.positionName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.positionIndex][_this.positionName].parameters));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.positionIndex][_this.positionName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.positionIndex][_this.positionName].info));

                    // GOTO POS X
                    term.push(function(command) {

                        if (command.match(/^(re)$/i)) {

                            term.pop();

                            history.enable();
                            return;
                        }

                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                            var posX = command;

                            _this.params.pos.x = posX;

                            term.echo("::GOTO POS X:: " + posX, {
                                finalize: function(div) {
                                    div.css("background-color", _this.logger.typerColorConfirm);
                                }
                            });

                            _this.logger.createNewTyper();

                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::GOTO POS X:: " + posX + "</span>")



                            // GOTO POS Y
                            term.push(function(command) {

                                if (command.match(/^(re)$/i)) {

                                    term.pop();

                                    history.enable();
                                    return;
                                }


                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                    var posY = command;

                                    _this.params.pos.y = posY;

                                    term.echo("::GOTO POS Y:: " + posY, {
                                        finalize: function(div) {
                                            div.css("background-color", _this.logger.typerColorConfirm);
                                        }
                                    });

                                    _this.logger.createNewTyper();

                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::GOTO POS Y:: " + posY + "</span>")




                                    // GOTO POS Z
                                    term.push(function(command) {

                                        if (command.match(/^(re)$/i)) {

                                            term.pop();

                                            history.enable();
                                            return;
                                        }


                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                            var posZ = command;

                                            _this.params.pos.z = posZ;

                                            term.echo("::GOTO POS Z:: " + posZ, {
                                                finalize: function(div) {
                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                }
                                            });

                                            _this.logger.createNewTyper();

                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::GOTO POS Z:: " + posZ + "</span>")


                                        // GOTO POS SPEED
                                    term.push(function(command) {

                                        if (command.match(/^(re)$/i)) {

                                            term.pop();

                                            history.enable();
                                            return;
                                        }


                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                            var speed = command;

                                            _this.params.speed = speed;

                                            term.echo("::GOTO POS SPEED:: " + speed, {
                                                finalize: function(div) {
                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                }
                                            });

                                            _this.logger.createNewTyper();

                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::GOTO POS SPEED:: " + speed + "</span>")



                                            _this.goToEvent.params = _this.params;
                                            _this.terminalDomElement.dispatchEvent(_this.goToEvent);

                                            term.pop();
                                            term.pop();
                                            term.pop();
                                            term.pop();
                                            term.pop();

                                            history.enable();

                                            return;

                                            }

                                    }, {
                                        prompt: '::speed: '
                                    });


                                        }

                                    }, {
                                        prompt: '::z: '
                                    });



                                }

                            }, {
                                prompt: '::y: '
                            });


                        }

                    }, {
                        prompt: '::x: '
                    });

                }


                //// GOTO ARTEFACT ////
                if (command.match(/^(artefact)$/i)) {

                    term.echo("::GOTO ARTEFACT::", {
                        finalize: function(div) {
                            div.css("background-color", _this.logger.typerColorConfirm);
                        }
                    });


                    _this.logger.createNewTyper();

                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));


                    // GOTO ARTEFACT TYPE
                    term.push(function(command) {

                        if (command.match(/^(re)$/i)) {

                            term.pop();

                            history.enable();
                            return;
                        }

                        if (command.match(/^(box|sphere|plane)$/i)) {

                            var type = command;

                            _this.params.type = type;


                            term.echo("::GO TO TYPE:: " + type, {
                                finalize: function(div) {
                                    div.css("background-color", _this.logger.typerColorConfirm);
                                }
                            });

                            _this.logger.createNewTyper();

                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::GO TO TYPE:: " + type + "</span>")



                            // GOTO ARTEFACT ID
                            term.push(function(command) {

                                if (command.match(/^(re)$/i)) {

                                    term.pop();

                                    history.enable();
                                    return;
                                }

                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                    var id = _this.params.type + "::" + parseInt(Math.abs(command));

                                    _this.params.id = id;

                                    term.echo("::GO TO ID:: " + id, {
                                        finalize: function(div) {
                                            div.css("background-color", _this.logger.typerColorConfirm);
                                        }
                                    });

                                    _this.logger.createNewTyper();

                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::GO TO ID:: " + id + "</span>")


                                    // GOTO POS SPEED
                                    term.push(function(command) {

                                        if (command.match(/^(re)$/i)) {

                                            term.pop();

                                            history.enable();
                                            return;
                                        }


                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                            var speed = command;

                                            _this.params.speed = speed;

                                            term.echo("::GOTO POS SPEED:: " + speed, {
                                                finalize: function(div) {
                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                }
                                            });

                                            _this.logger.createNewTyper();

                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::GOTO POS SPEED:: " + speed + "</span>")


                                    // Dispatch the event.
                                    _this.goToEvent.params = _this.params;
                                    _this.terminalDomElement.dispatchEvent(_this.goToEvent);

                                    term.pop();
                                    term.pop();
                                    term.pop();
                                    term.pop();

                                    history.enable();

                                    return;


 }
                            }, {
                                prompt: '::speed: '
                            });



                                }
                            }, {
                                prompt: '::id: '
                            });



                        }

                    }, {
                        prompt: '::artefact: '
                    });



                }

            }, {
                prompt: '::goto: '
            });


        }

        (function() {
            Math.clamp = function(a, b, c) {
                return Math.max(b, Math.min(c, a));
            }
        })();

        Number.prototype.map = function(in_min, in_max, out_min, out_max) {
            var val = (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
            if (val < out_min) val = out_min;
            if (val > out_max) val = out_max;
            return val;
        }

    }

}
export default goToCmd;