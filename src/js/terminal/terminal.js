import jQuery from 'jquery';
import TerminalJS from 'jquery.terminal';
import Logger from './logger';
import SignalCmd from './signalCmd';
import DebugCmd from './debugCmd';
import AsciiCmd from './asciiCmd';
import GoToCmd from './goToCmd';
import CreateBoxCmd from './createBoxCmd';
import DeleteBoxCmd from './deleteBoxCmd';
import CreateSphereCmd from './createSphereCmd';
import DeleteSphereCmd from './deleteSphereCmd';
import CreatePlaneCmd from './createPlaneCmd';
import DeletePlaneCmd from './deletePlaneCmd';

class Terminal {

    constructor(username) {

        var _this = this;

        // DOM ELEMENT
        this.domElement = document.getElementById('terminal');

        // LOGGER
        this.logger = new Logger(document.querySelector('#logger'));
        console.log("::COMMANDS::", this.logger.commands);

        // COMMANDS        

        this.signalCmd = new SignalCmd(this.domElement, this.logger);

        this.debugCmd = new DebugCmd(this.domElement, this.logger);

        this.asciiCmd = new AsciiCmd(this.domElement, this.logger);

        this.goToCmd = new GoToCmd(this.domElement, this.logger);

        this.createBoxCmd = new CreateBoxCmd(this.domElement, this.logger);
        this.deleteBoxCmd = new DeleteBoxCmd(this.domElement, this.logger);

        this.createSphereCmd = new CreateSphereCmd(this.domElement, this.logger);
        this.deleteSphereCmd = new DeleteSphereCmd(this.domElement, this.logger);

        this.createPlaneCmd = new CreatePlaneCmd(this.domElement, this.logger);
        this.deletePlaneCmd = new DeletePlaneCmd(this.domElement, this.logger);

        // TERMINAL
        jQuery(function($, undefined) {

          $('#terminal').terminal(function(command, term) {

            // LOGGER
            _this.logger.listen(command);

            // SIGNAL
            _this.signalCmd.listen(command, term);

            // DEBUG
            _this.debugCmd.listen(command, term);

            // ASCII
            _this.asciiCmd.listen(command, term);

            // GOTO
            _this.goToCmd.listen(command, term);

            // BOX
            _this.createBoxCmd.listen(command, term);
            _this.deleteBoxCmd.listen(command, term);

            // SPHERE
            _this.createSphereCmd.listen(command, term);
            _this.deleteSphereCmd.listen(command, term);

            // PLANE
            _this.createPlaneCmd.listen(command, term);
            _this.deletePlaneCmd.listen(command, term);

          }, {
              greetings: "Hi " + username + ", welcome to ::COLON::",
              name: 'terminal',
              width: 300,
              height: 200,
              prompt: "::" + username + " > ",
              onBlur: function() {
                return false;
              },
              onAfterCommand: function() {
                $("#terminal").removeClass("flash");
                setTimeout(function() {
                    $("#terminal").addClass("flash");
                }, 1);
              }
          });

      });

    }  

    scrollLog() {

      if ( !this.logger.isInspect ) {
        this.logger.domElement.scrollTop = this.logger.domElement.scrollHeight;
      }

    } 

}
export default Terminal;