class deleteBoxCmd {

    constructor(terminalDomElement, logger) {

        var _this = this;

        // PARAMS
        this.params = {
            num: 1,
            id: 0
        };

        this.boxNumMax = 1000;

        // DOM ELEMENT
        this.terminalDomElement = terminalDomElement;

        // LOGGER
        this.logger = logger;

        this.commandName = "delete";
        this.artefactName = "box";
        this.artefactIndex = 1;

        // EVENT
        this.deleteBoxEvent = new CustomEvent('deleteBox');

    }

    listen(command, term) {

        var _this = this;

        if (command == 'delete') {

            var history = term.history();

            history.disable();


            term.push(function(command) {

                if (command.match(/^(re)$/i)) {

                    term.pop();

                    history.enable();
                    return;
                }


                //// BOX ////
                if (command.match(/^(box)$/i)) {

                    term.echo("::BOX::", {
                        finalize: function(div) {
                            div.css("background-color", _this.logger.typerColorConfirm);
                        }
                    });


                    _this.logger.createNewTyper();

                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                    _this.logger.typer.line();
                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));


                    // BOX NUM
                    term.push(function(command) {

                        if (command.match(/^(re)$/i)) {

                            term.pop();

                            history.enable();
                            return;
                        }

                        if (command.match(/^[alAL0-9_#(),.-]*$/)) {

                            var num;
                            if (command == "all" || command == "ALL") {
                                num = command;
                            } else {

                                num = parseInt(Math.abs(command));
                                if (num > _this.boxNumMax) num = _this.boxNumMax;
                            }


                            _this.params.num = num;

                            term.echo("::BOX NUM TO DELETE:: " + num, {
                                finalize: function(div) {
                                    div.css("background-color", _this.logger.typerColorConfirm);
                                }
                            });

                            _this.logger.createNewTyper();

                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX NUM TO DELETE:: " + num + "</span>")

                            if (num > 1 || num == "all" || num == "ALL") {

                                // Dispatch the event.
                                _this.deleteBoxEvent.params = _this.params;
                                _this.terminalDomElement.dispatchEvent(_this.deleteBoxEvent);

                                term.pop();
                                term.pop();

                                history.enable();

                                return;
                            }

                            // BOX ID
                            term.push(function(command) {

                                if (command.match(/^(re)$/i)) {

                                    term.pop();

                                    history.enable();
                                    return;
                                }

                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                    var id = parseInt(Math.abs(command));

                                    _this.params.id = id;

                                    term.echo("::BOX ID TO DELETE:: " + num, {
                                        finalize: function(div) {
                                            div.css("background-color", _this.logger.typerColorConfirm);
                                        }
                                    });

                                    _this.logger.createNewTyper();

                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::BOX ID TO DELETE:: " + id + "</span>")



                                    // Dispatch the event.
                                    _this.deleteBoxEvent.params = _this.params;
                                    _this.terminalDomElement.dispatchEvent(_this.deleteBoxEvent);

                                    term.pop();
                                    term.pop();
                                    term.pop();

                                    history.enable();

                                    return;


                                }




                            }, {
                                prompt: '::id: '
                            });



                        }

                    }, {
                        prompt: '::num: '
                    });



                }

            }, {
                prompt: '::delete: '
            });


        }

        (function() {
            Math.clamp = function(a, b, c) {
                return Math.max(b, Math.min(c, a));
            }
        })();

        Number.prototype.map = function(in_min, in_max, out_min, out_max) {
            var val = (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
            if (val < out_min) val = out_min;
            if (val > out_max) val = out_max;
            return val;
        }

    }

}
export default deleteBoxCmd;