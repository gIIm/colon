class DebugCmd {

    constructor(terminalDomElement, logger) {

        var _this = this;

        // PARAMS
        this.params = {
            isOn: false,
            isOff: true
        };

        // DOM ELEMENT
        this.terminalDomElement = terminalDomElement;

        // LOGGER
        this.logger = logger;

        // EVENT
        this.debugEvent = new CustomEvent('debug');

    }

    listen(command, term) {

        var _this = this;

        if (command == 'debug') {

            var history = term.history();

            history.disable();

            term.push(function(command) {

                if (command.match(/^(re)$/i)) {

                    term.pop();

                    history.enable();
                    return;
                }


                if (command.match(/^(on|true)$/i)) {

                    if (_this.params.isOn) {
                        //term.echo("::SIGNAL IS ALREADY ON::");
                        term.echo("::DEBUG MODE IS ALREADY ON::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorError);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color: " + _this.logger.typerColorError + "'>::DEBUG MODE IS ALREADY ON::</span>")

                        return;
                    } else {

                        // term.echo('::AUDIO SIGNAL ON::');
                        term.echo("::DEBUG MODE ON::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorConfirm);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.params.isOn = true;
                        _this.params.isOff = false;

                        // Dispatch the event.
                        _this.debugEvent.params = _this.params;
                        _this.terminalDomElement.dispatchEvent(_this.debugEvent);

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::DEBUG MODE ON::</span>")
                        _this.logger.typer.line();
                        _this.logger.typer.line("::debug::on: " + _this.params.isOn);
                        _this.logger.typer.line();
                        _this.logger.typer.line("::debug::off: " + _this.params.isOff);

                    }

                } else if (command.match(/^(off|false)$/i)) {

                    if (_this.params.isOff) {
                        // term.echo("::SIGNAL IS ALREADY OFF::");
                        term.echo("::DEBUG MODE IS ALREADY OFF::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorError);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color: " + _this.logger.typerColorError + "'>::DEBUG MODE IS ALREADY OFF::</span>")

                        return;
                    } else {

                        // term.echo('::AUDIO SIGNAL OFF::');
                        term.echo("::DEBUG MODE OFF::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorConfirm);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.params.isOn = false;
                        _this.params.isOff = true;

                        // Dispatch the event.
                        _this.debugEvent.params = _this.params;
                        _this.terminalDomElement.dispatchEvent(_this.debugEvent);

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color: " + _this.logger.typerColorConfirm + "'>::DEBUG MODE OFF::</span>")
                        _this.logger.typer.line();
                        _this.logger.typer.line("::debug::on: " + _this.params.isOn);
                        _this.logger.typer.line();
                        _this.logger.typer.line("::debug::off: " + _this.params.isOff);

                    }

                }

            }, {

                prompt: '::state: '

            });

        }

    }

}
export default DebugCmd;