class AsciiCmd {

    constructor(terminalDomElement, logger) {

        var _this = this;

        // PARAMS
        this.params = {
            isOn: false,
            isOff: true
        };

        // DOM ELEMENT
        this.terminalDomElement = terminalDomElement;

        // LOGGER
        this.logger = logger;

        // EVENT
        this.asciiEvent = new CustomEvent('ascii');

    }

    listen(command, term) {

        var _this = this;

        if (command == 'ascii') {

            var history = term.history();

            history.disable();

            term.push(function(command) {

                if (command.match(/^(re)$/i)) {

                    term.pop();

                    history.enable();
                    return;
                }


                if (command.match(/^(on|true)$/i)) {

                    if (_this.params.isOn) {
                        //term.echo("::SIGNAL IS ALREADY ON::");
                        term.echo("::ASCII MODE IS ALREADY ON::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorError);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color: " + _this.logger.typerColorError + "'>::ASCII MODE IS ALREADY ON::</span>")

                        return;
                    } else {

                        // term.echo('::AUDIO SIGNAL ON::');
                        term.echo("::ASCII MODE ON::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorConfirm);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.params.isOn = true;
                        _this.params.isOff = false;

                        // Dispatch the event.
                        _this.asciiEvent.params = _this.params;
                        _this.terminalDomElement.dispatchEvent(_this.asciiEvent);

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::ASCII MODE ON::</span>")
                        _this.logger.typer.line();
                        _this.logger.typer.line("::ascii::on: " + _this.params.isOn);
                        _this.logger.typer.line();
                        _this.logger.typer.line("::ascii::off: " + _this.params.isOff);

                    }

                } else if (command.match(/^(off|false)$/i)) {

                    if (_this.params.isOff) {
                        // term.echo("::SIGNAL IS ALREADY OFF::");
                        term.echo("::ASCII MODE IS ALREADY OFF::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorError);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color: " + _this.logger.typerColorError + "'>::ASCII MODE IS ALREADY OFF::</span>")

                        return;
                    } else {

                        // term.echo('::AUDIO SIGNAL OFF::');
                        term.echo("::ASCII MODE OFF::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorConfirm);
                            }
                        });

                        term.pop();
                        history.enable();

                        _this.params.isOn = false;
                        _this.params.isOff = true;

                        // Dispatch the event.
                        _this.asciiEvent.params = _this.params;
                        _this.terminalDomElement.dispatchEvent(_this.asciiEvent);

                        _this.logger.createNewTyper();

                        _this.logger.typer.line("<span style='background-color: " + _this.logger.typerColorConfirm + "'>::ASCII MODE OFF::</span>")
                        _this.logger.typer.line();
                        _this.logger.typer.line("::ascii::on: " + _this.params.isOn);
                        _this.logger.typer.line();
                        _this.logger.typer.line("::ascii::off: " + _this.params.isOff);

                    }

                }

            }, {

                prompt: '::state: '

            });

        }

    }

}
export default AsciiCmd;