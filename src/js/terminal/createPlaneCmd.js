class CreatePlaneCmd {

    constructor(terminalDomElement, logger) {

        var _this = this;

        // PARAMS
        this.params = {
            mode: "face",
            num: 1,
            width: 100,
            height: 100,
            segments: 1,
            posX: Math.random() * 600 - 300,
            posY: Math.random() * 600 - 300,
            posZ: Math.random() * -500,
            rotX: Math.random() * Math.PI * 2,
            rotY: Math.random() * Math.PI * 2,
            rotZ: Math.random() * Math.PI * 2,
            color: "#fff"
        };

        this.planeNumMax = 1000;
        this.planeRotMin = 0.0;
        this.planeRotMax = 0.15;

        // DOM ELEMENT
        this.terminalDomElement = terminalDomElement;

        // LOGGER
        this.logger = logger;

        this.commandName = "create";
        this.artefactName = "plane";
        this.artefactIndex = 3;

        // EVENT
        this.createPlaneEvent = new CustomEvent('createPlane');

    }

    listen(command, term) {

        var _this = this;

        if (command == 'create::') {

            var history = term.history();

            history.disable();


            term.push(function(command) {

                    if (command.match(/^(re)$/i)) {

                        term.pop();

                        history.enable();
                        return;
                    }


                    //// PLANE ////
                    if (command.match(/^(plane)$/i)) {

                        term.echo("::PLANE::", {
                            finalize: function(div) {
                                div.css("background-color", _this.logger.typerColorConfirm);
                            }
                        });


                        _this.logger.createNewTyper();

                        if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                        _this.logger.typer.line();
                        if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                        _this.logger.typer.line();
                        if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));

                        // PLANE MODE
                        term.push(function(command) {

                                if (command.match(/^(re)$/i)) {

                                    term.pop();

                                    history.enable();
                                    return;
                                }

                                if (command.match(/^(point|points|wireframe|face|faces)$/i)) {

                                    var mode = command;

                                    _this.params.mode = mode;

                                    term.echo("::PLANE MODE::" + mode, {
                                        finalize: function(div) {
                                            div.css("background-color", _this.logger.typerColorConfirm);
                                        }
                                    });


                                    _this.logger.createNewTyper();

                                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command) _this.logger.typer.line(JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].command));
                                    _this.logger.typer.line();
                                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters) _this.logger.typer.line("parameters: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].parameters));
                                    _this.logger.typer.line();
                                    if (_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info) _this.logger.typer.line("info: <br>" + JSON.stringify(_this.logger.commands[_this.commandName][_this.artefactIndex][_this.artefactName].info));




                                    // PLANE NUM
                                    term.push(function(command) {

                                            if (command.match(/^(re)$/i)) {

                                                term.pop();

                                                history.enable();
                                                return;
                                            }


                                            if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                var num = Math.abs(command);
                                                if (num > _this.planeNumMax) num = _this.planeNumMax;

                                                _this.params.num = num;

                                                term.echo("::PLANE NUM:: " + num, {
                                                    finalize: function(div) {
                                                        div.css("background-color", _this.logger.typerColorConfirm);
                                                    }
                                                });

                                                _this.logger.createNewTyper();

                                                _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE NUM:: " + num + "</span>")

                                                if (num > 1) {

                                                    // Dispatch the event.
                                                    _this.createPlaneEvent.params = _this.params;
                                                    _this.terminalDomElement.dispatchEvent(_this.createPlaneEvent);

                                                    //term.pop();
                                                    term.pop();
                                                    term.pop();
                                                    term.pop();

                                                    history.enable();

                                                    return;
                                                }




                                                // PLANE WIDTH
                                                term.push(function(command) {

                                                       
                                             

                                                             if (command.match(/^(re)$/i)) {

                                                            term.pop();

                                                            history.enable();
                                                            return;
                                                        }

                                                        var width = Math.abs(command);

                                                        _this.params.width = width;

                                                        term.echo("::PLANE WIDTH:: " + width, {
                                                            finalize: function(div) {
                                                                div.css("background-color", _this.logger.typerColorConfirm);
                                                            }
                                                        });

                                                        _this.logger.createNewTyper();

                                                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE WIDTH:: " + width + "</span>")


                                                // PLANE HEIGHT
                                                term.push(function(command) {

                                                       
                                             

                                                             if (command.match(/^(re)$/i)) {

                                                            term.pop();

                                                            history.enable();
                                                            return;
                                                        }

                                                        var height = Math.abs(command);

                                                        _this.params.height = height;

                                                        term.echo("::PLANE HEIGHT:: " + height, {
                                                            finalize: function(div) {
                                                                div.css("background-color", _this.logger.typerColorConfirm);
                                                            }
                                                        });

                                                        _this.logger.createNewTyper();

                                                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE HEIGHT:: " + height + "</span>")





                                                            // PLANE SEGMENTS
                                                            term.push(function(command) {

                                                                    if (command.match(/^(re)$/i)) {

                                                                        term.pop();

                                                                        history.enable();
                                                                        return;
                                                                    }

                                                                    if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                        var segments = Math.abs(command);

                                                                        _this.params.segments = segments;

                                                                        term.echo("::PLANE SEGMENTS:: " + segments, {
                                                                            finalize: function(div) {
                                                                                div.css("background-color", _this.logger.typerColorConfirm);
                                                                            }
                                                                        });

                                                                        _this.logger.createNewTyper();

                                                                        _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE SEGMENTS:: " + segments + "</span>")






                                                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {



                                                                            // PLANE POS X
                                                                            term.push(function(command) {

                                                                                if (command.match(/^(re)$/i)) {

                                                                                    term.pop();

                                                                                    history.enable();
                                                                                    return;
                                                                                }


                                                                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                                    var posX = command;

                                                                                    _this.params.posX = posX;

                                                                                    term.echo("::PLANE POS X:: " + posX, {
                                                                                        finalize: function(div) {
                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                        }
                                                                                    });

                                                                                    _this.logger.createNewTyper();

                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE POS X:: " + posX + "</span>")




                                                                                    // PLANE POS Y
                                                                                    term.push(function(command) {

                                                                                        if (command.match(/^(re)$/i)) {

                                                                                            term.pop();

                                                                                            history.enable();
                                                                                            return;
                                                                                        }


                                                                                        if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                                            var posY = command;

                                                                                            _this.params.posY = posY;

                                                                                            term.echo("::PLANE POS Y:: " + posY, {
                                                                                                finalize: function(div) {
                                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                }
                                                                                            });

                                                                                            _this.logger.createNewTyper();

                                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE POS Y:: " + posY + "</span>")




                                                                                            // PLANE POS Z
                                                                                            term.push(function(command) {

                                                                                                if (command.match(/^(re)$/i)) {

                                                                                                    term.pop();

                                                                                                    history.enable();
                                                                                                    return;
                                                                                                }


                                                                                                if (command.match(/^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/)) {

                                                                                                    var posZ = command;

                                                                                                    _this.params.posZ = posZ;

                                                                                                    term.echo("::PLANE POS Z:: " + posZ, {
                                                                                                        finalize: function(div) {
                                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                        }
                                                                                                    });

                                                                                                    _this.logger.createNewTyper();

                                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE POS Z:: " + posZ + "</span>")




                                                                                                    // PLANE ROT X
                                                                                                    term.push(function(command) {

                                                                                                        if (command.match(/^(re)$/i)) {

                                                                                                            term.pop();

                                                                                                            history.enable();
                                                                                                            return;
                                                                                                        }

                                                                                                        if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                                            var rotX = Number(command);
                                                                                                            rotX = rotX.map(0, 10, _this.planeRotMin, _this.planeRotMax);

                                                                                                            _this.params.rotX = rotX;

                                                                                                            term.echo("::PLANE ROT X:: " + rotX, {
                                                                                                                finalize: function(div) {
                                                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                }
                                                                                                            });

                                                                                                            _this.logger.createNewTyper();

                                                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE ROT X:: " + rotX + "</span>");



                                                                                                            // PLANE ROT Y
                                                                                                            term.push(function(command) {

                                                                                                                if (command.match(/^(re)$/i)) {

                                                                                                                    term.pop();

                                                                                                                    history.enable();
                                                                                                                    return;
                                                                                                                }

                                                                                                                if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                                                    var rotY = Number(command);
                                                                                                                    rotY = rotY.map(0, 10, _this.planeRotMin, _this.planeRotMax);

                                                                                                                    _this.params.rotY = rotY;

                                                                                                                    term.echo("::PLANE ROT Y:: " + rotY, {
                                                                                                                        finalize: function(div) {
                                                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                        }
                                                                                                                    });

                                                                                                                    _this.logger.createNewTyper();

                                                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE ROT Y:: " + rotY + "</span>");




                                                                                                                    // PLANE ROT Z
                                                                                                                    term.push(function(command) {

                                                                                                                        if (command.match(/^(re)$/i)) {

                                                                                                                            term.pop();

                                                                                                                            history.enable();
                                                                                                                            return;
                                                                                                                        }

                                                                                                                        if (command.match(/^(?=.)([+-]?([a-zA-Z0-9(). *-]*)(\.([0-9]+))?)$/)) {

                                                                                                                            var rotZ = Number(command);
                                                                                                                            rotZ = rotZ.map(0, 10, _this.planeRotMin, _this.planeRotMax);

                                                                                                                            _this.params.rotZ = rotZ;

                                                                                                                            term.echo("::PLANE ROT Z:: " + rotZ, {
                                                                                                                                finalize: function(div) {
                                                                                                                                    div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                                }
                                                                                                                            });

                                                                                                                            _this.logger.createNewTyper();

                                                                                                                            _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE ROT Z:: " + rotZ + "</span>")




                                                                                                                            // COLOR
                                                                                                                            term.push(function(command) {

                                                                                                                                if (command.match(/^(re)$/i)) {

                                                                                                                                    term.pop();

                                                                                                                                    history.enable();
                                                                                                                                    return;
                                                                                                                                }

                                                                                                                                if (command.match(/^[a-zA-Z0-9_#(),.-]*$/)) {

                                                                                                                                    var color = command;

                                                                                                                                    _this.params.color = color;

                                                                                                                                    term.echo("::PLANE COLOR:: " + color, {
                                                                                                                                        finalize: function(div) {
                                                                                                                                            div.css("background-color", _this.logger.typerColorConfirm);
                                                                                                                                        }
                                                                                                                                    });

                                                                                                                                    _this.logger.createNewTyper();

                                                                                                                                    _this.logger.typer.line("<span style='background-color:" + _this.logger.typerColorConfirm + "'>::PLANE COLOR:: " + color + "</span>")



                                                                                                                                    _this.createPlaneEvent.params = _this.params;
                                                                                                                                    _this.terminalDomElement.dispatchEvent(_this.createPlaneEvent);

                                                                                                                                    term.pop();                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();
                                                                                                                                    term.pop();

                                                                                                                                    history.enable();

                                                                                                                                    return;




                                                                                                                                }
                                                                                                                            }, {
                                                                                                                                prompt: '::color: '
                                                                                                                            });




                                                                                                                        }
                                                                                                                    }, {
                                                                                                                        prompt: '::rot::z: '
                                                                                                                    });




                                                                                                                }
                                                                                                            }, {
                                                                                                                prompt: '::rot::y: '
                                                                                                            });




                                                                                                        }
                                                                                                    }, {
                                                                                                        prompt: '::rot::x: '
                                                                                                    });




                                                                                                }
                                                                                            }, {
                                                                                                prompt: '::pos::z: '
                                                                                            });




                                                                                        }
                                                                                    }, {
                                                                                        prompt: '::pos::y: '
                                                                                    });




                                                                                }
                                                                            }, {
                                                                                prompt: '::pos::x: '
                                                                            });




                                                                        }














                                                                    }

                                                                    }, {
                                                                        prompt: '::segments: '
                                                                    });
    
                                                        
                                                        }, {
                                                            prompt: '::height: '
                                                        });
    

                                                        }, {
                                                            prompt: '::width: '
                                                        });



                                                }
                                            }, {
                                                prompt: '::num: '
                                            });


                                    }
                                }, {
                                    prompt: '::mode: '
                                });

                        }

                    }, {
                        prompt: '::create: '
                    });


            }

            (function() {
                Math.clamp = function(a, b, c) {
                    return Math.max(b, Math.min(c, a));
                }
            })();

            Number.prototype.map = function(in_min, in_max, out_min, out_max) {
                var val = (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
                if (val < out_min) val = out_min;
                if (val > out_max) val = out_max;
                return val;
            }

        }

    }
    export default CreatePlaneCmd;