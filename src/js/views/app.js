import 'three'
import 'three/examples/js/controls/OrbitControls'
import 'three/examples/js/controls/VRControls'
import '../fx/ascii'
import TWEEN from 'tween.js'
import stats from 'stats-js'

class App {

    constructor() {

        var _this = this;

        // DEBUG MODE
        this.debugMode = false;
        this.showStats = false;

        // WINDOW
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.windowHalfX = this.width / 2;
        this.windowHalfY = this.height / 2;

        // SCENE
        this._scene = new THREE.Scene();
        this._scene.fog = new THREE.FogExp2(0x0d2738, 0.0005);

        // CAMERA
        this._camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 10000);
        this._camera.position.z = 400;

        this.cameraDebug = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 10000 );
        this.cameraDebugHelper = new THREE.CameraHelper( this._camera );
        this.cameraDebug.rotation.y = Math.PI;
        this._scene.add( this.cameraDebugHelper );

        // LIGHT
        this.ambientLight = new THREE.AmbientLight(0xf0f0f0, 1);
        this._scene.add(this.ambientLight);

        this.light = new THREE.PointLight(0xffffff, 1, 0, 2);
        this.light.position.copy(this._camera.position);
        this._scene.add(this.light);

        // RENDERER
        this._renderer = new THREE.WebGLRenderer({
            alpha: true
        });
        this._renderer.setSize(this.width, this.height);
        this._renderer.setPixelRatio(window.devicePixelRatio);
        this._renderer.setClearColor(0x000000, 0);
        this._renderer.domElement.id = "webglRenderer";
        document.body.appendChild(this._renderer.domElement);

        this.rendererDebug = new THREE.WebGLRenderer({
            alpha: true
        });
        this.rendererDebug.setSize(300, 240);
        this.rendererDebug.setPixelRatio(window.devicePixelRatio);
        this.rendererDebug.setClearColor(0x000000, 0);
        this.rendererDebug.domElement.id = "webglRendererDebug";
        document.body.appendChild(this.rendererDebug.domElement);

        // ASCII EFFECT
        let asciiCharset = ' .,:;=|iI+hHOE#`$';
        let asciiOptions = {
            resolution: 0.15,
            scale: 1,
            alpha: false,
            block: false,
            invert: true
        }
        this.asciiEffect = new THREE.AsciiEffect( this._renderer, asciiCharset, asciiOptions );
        this.asciiEffect.setSize( this.width, this.height );
        document.body.appendChild( this.asciiEffect.domElement );

        this.asciiMode = false;

        // CONTROLS
        this.controls = new THREE.OrbitControls(this._camera, this._renderer.domElement);
        this.controls.enableZoom = true;
        this.controls.enableRotate = true;
        this.controls.enablePan = true;
        this.controls.autoRotate = false;
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.24;

        this.controlsAscii = new THREE.OrbitControls(this._camera, this.asciiEffect.domElement);
        this.controlsAscii.enableZoom = true;
        this.controlsAscii.enableRotate = true;
        this.controlsAscii.enablePan = true;
        this.controlsAscii.autoRotate = false;
        this.controlsAscii.enableDamping = true;
        this.controlsAscii.dampingFactor = 0.24;

        // RAYCAST
        this.raycaster = new THREE.Raycaster();
        this.intersects = null;

        // EASING
        this.easing = TWEEN.Easing.Cubic.InOut;
        this.easingSpeed = 1500;

        // STATS
        if (this.showStats) {

            this.stats = new stats();
            var statsEl = document.getElementById("stats");
            statsEl.appendChild(this.stats.domElement);

        }

        document.querySelector("#terminal").addEventListener('debug', function (e) {
            _this.getDebugState(e);
        }, false);

        document.querySelector("#terminal").addEventListener('ascii', function (e) {
            _this.getAsciiState(e);
        }, false);

        window.addEventListener('resize', this.onWindowResize.bind(this), false);

    }

    get renderer() {

        return this._renderer;

    }

    get camera() {

        return this._camera;

    }

    get scene() {

        return this._scene;

    }

    getDebugState(e) {
        console.log("DEBUG MODE CHANGE", e);

        var _this = this;

          if ( e.params.isOn ) {
                this.debugMode = true;
            } else if ( e.params.isOff) {
                this.debugMode = false;
            }
    }

    getAsciiState(e) {
        console.log("ASCII MODE CHANGE", e);

        var _this = this;

          if ( e.params.isOn ) {
                this.asciiMode = true;
            } else if ( e.params.isOff) {
                this.asciiMode = false;
            }
    }

    onWindowResize() {

        this._camera.aspect = window.innerWidth / window.innerHeight;
        this._camera.updateProjectionMatrix();

        this.cameraDebug.aspect = window.innerWidth / window.innerHeight;
        this.cameraDebug.updateProjectionMatrix();

        this._renderer.setSize(window.innerWidth, window.innerHeight);

        this.rendererDebug.setSize(300, 240);

        this.asciiEffect.setSize( window.innerWidth, window.innerHeight );

    }

    animate(timestamp) {
        //requestAnimationFrame( this.animate.bind(this) );

        if (this.debugMode) {
            
            this.cameraDebug.lookAt(this._camera.position);

            this.cameraDebugHelper.visible = true;
            this.cameraDebugHelper.update();

            this.rendererDebug.render(this._scene, this.cameraDebug);

        } else {
            this.cameraDebugHelper.visible = false;
        }

        if (this.asciiMode) {
            this.controlsAscii.update();

            this._renderer.domElement.style.display = "none";
            this.asciiEffect.domElement.style.display = "block";

        } else {

            this.controls.update();

            this._renderer.domElement.style.display = "block";
            this.asciiEffect.domElement.style.display = "none";        
        }

        //this._renderer.render(this._scene, this._camera);

        // this.light.position.copy(this._camera.position);

        //this._camera.lookAt( new THREE.Vector3( this._camera.position.x, this._camera.position.y, this._camera.position.z ) );

        //this.asciiEffect.render( this._scene, this._camera );

    }


}
export default App;