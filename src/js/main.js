import * as THREE from 'three'
import TWEEN from 'tween.js'
import dat from 'dat-gui'
import Terminal from 'terminal/terminal'
import Audio from 'helpers/audio'
import App from 'views/app'
import Effect from 'helpers/effect'
import Box from 'helpers/box'
import Sphere from 'helpers/sphere'
import Plane from 'helpers/plane'

class Main {

    constructor() {
        var _this = this;

        new Promise((resolve, reject) => {
            // LOGIN
            this.username;

            var appContainerElement = document.getElementById("app-container");

            var loginContainerElement = document.getElementById("login-container");
    
            var loginInputElement = document.getElementById("login-input");
            loginInputElement.focus();
            loginInputElement.onkeypress = function(e){
                if(e.keyCode == 13) {
                    _this.username = loginInputElement.value ? loginInputElement.value : "user";

                    loginContainerElement.classList.add("fade-out");
                    appContainerElement.style.display = "block";
    
                    loginContainerElement.addEventListener("animationend", function(){
                        loginContainerElement.style.display = "none";
                        appContainerElement.classList.add("fade-in");
                        
                        resolve();
                    });
    
                    return false;
                }
            };            

            var loginEnterElement = document.getElementById("login-enter");
            loginEnterElement.onclick = function(){
                _this.username = loginInputElement.value ? loginInputElement.value : "user";

                loginContainerElement.classList.add("fade-out");
                appContainerElement.style.display = "block";

                loginContainerElement.addEventListener("animationend", function(){
                    loginContainerElement.style.display = "none";
                    appContainerElement.classList.add("fade-in");
                    
                    resolve();
                });
            };            
        }).then(() => {
            // TERMINAL
            this.terminal = new Terminal(this.username);

            // AUDIO
            this.audio = new Audio(this.terminal);

            // APP
            this.app = new App();

            // GUI
            this.gui = new dat.GUI({autoPlace: false});
            this.showGUI = false;
            if (!this.showGUI) this.gui.__proto__.constructor.toggleHide();
            this.guiContainer = document.getElementById('gui');
            if (this.showGUI) this.guiContainer.appendChild(this.gui.domElement);

            // EFFECT
            this.effect = new Effect(this.app, this.gui);

            // // CUBES
            // this.cubes = [];

            // this.material = new THREE.MeshPhongMaterial({
            //     color: 0x3a9ceb
            // });
            // let c;
            // for (let i = 0; i < 500; i++) {
            //     c = this.addCube();
            //     this.cubes.push(c);
            //     this.app._scene.add(c);
            // }

            // BOX
            this.box = new Box(this.app, this.terminal, this.gui);

            // SPHERE
            this.sphere = new Sphere(this.app, this.terminal, this.gui);

            // PLANE
            this.plane = new Plane(this.app, this.terminal, this.gui);
        }).then(() => {
            this.animate();
        }).catch((error) => {
            console.error(error);
        }).finally(() => {
        
        });  
    }

    addCube() {

        let cube = new THREE.Mesh(new THREE.BoxGeometry(20, 20, 20), this.material);

        cube.position.set(
            Math.random() * 600 - 300,
            Math.random() * 600 - 300,
            Math.random() * -500
        );

        cube.rotation.set(
            Math.random() * Math.PI * 2,
            Math.random() * Math.PI * 2,
            Math.random() * Math.PI * 2
        );
        return cube;

    }

    animate() {

        var time = Date.now() * 0.00005;

        // AUDIO
        this.audio.getAverageFFT();
        this.app._renderer.setClearColor(this.audio.fftColor, 1.0);

        // // CUBES
        // for (let i = 0; i < this.cubes.length; i++) {

        //     // this.xSpeed[i] += 0.005;
        //     // this.ySpeed[i] += 0.005;

        //     // this.group.children[i].position.x = this.group.children[i].position.x + Math.cos(this.xSpeed[i]);
        //     // this.group.children[i].position.y = this.group.children[i].position.y + Math.sin(this.ySpeed[i]);

        //     // this.cubes[i].rotation.y += 0.01 + ((i - this.cubes.length) * 0.00001);
        //     // this.cubes[i].rotation.x += 0.01 + ((i - this.cubes.length) * 0.00001);

        // }
        
        this.box.update();

        this.sphere.update();

        this.plane.update();

        // TERMINAL
        this.terminal.scrollLog();

        // APP
        
        if (this.app.asciiMode) {
            this.app.asciiEffect.render( this.app._scene, this.app._camera );

        } else {
            this.app._renderer.render(this.app._scene, this.app._camera);
        }
        
        
        this.app.animate();

        // EFFECT
        if (this.effect.params.usePostProcessing) {
            this.effect.update();
        }

        // TWEEN
        TWEEN.update();
        
        requestAnimationFrame(this.animate.bind(this));

        // STATS
        if (this.app.showStats && this.app.stats) this.app.stats.update();

        (function() {
            Math.clamp = function(a, b, c) {
                return Math.max(b, Math.min(c, a));
            }
        })();

    }

}
export default Main;