import WAGNER from '@superguigui/wagner/'
import BoxBlurPass from '@superguigui/wagner/src/passes/box-blur/BoxBlurPass'
import FXAAPass from '@superguigui/wagner/src/passes/fxaa/FXAAPass'
import ZoomBlurPassfrom from '@superguigui/wagner/src/passes/zoom-blur/ZoomBlurPass'
import MultiPassBloomPass from '@superguigui/wagner/src/passes/bloom/MultiPassBloomPass'

class Effect {

    constructor(app, gui) {

        var _this = this;

        this.params = {
            usePostProcessing: false,
            useFXAA: true,
            useBlur: false,
            useBloom: false
        };

        this.app = app;

        this.gui = gui;

        this.initPostprocessing();

        this.addToGui(this.gui);

    }

    initPostprocessing() {

        this.app._renderer.autoClearColor = true;
        this.composer = new WAGNER.Composer(this.app._renderer);
        this.fxaaPass = new FXAAPass();
        this.boxBlurPass = new BoxBlurPass(3, 3);
        this.bloomPass = new MultiPassBloomPass({
            blurAmount: 2,
            applyZoomBlur: true
        });

    }

    addToGui(gui) {

        gui.add(this.params, 'usePostProcessing');
        gui.add(this.params, 'useFXAA');
        gui.add(this.params, 'useBlur');
        gui.add(this.params, 'useBloom');
        
        return gui;
    }

    update() {

        if (this.params.usePostProcessing) {
            this.composer.reset();
            this.composer.render(this.app._scene, this.app._camera);
            if (this.params.useFXAA) this.composer.pass(this.fxaaPass);
            if (this.params.useBlur) this.composer.pass(this.boxBlurPass);
            if (this.params.useBloom) this.composer.pass(this.bloomPass);
            this.composer.toScreen();
        }


    }

}
export default Effect;