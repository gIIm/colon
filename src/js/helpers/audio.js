
var getUserMedia = require('get-user-media-promise');
var MicrophoneStream = require('microphone-stream');

var fft = require('fft-js').fft
var fftUtil = require('fft-js').util;

class Audio {

    constructor(terminal) {

        var _this = this;

        // AUDIO
        this.audioData = null;
        this.fftAverage = 0;
        this.fftColor = 0.0;
        this.fftSensitivity = 0.5;

        this.micStream = null;

        // TERMINAL
        this.terminal = terminal;
        // console.log(this.terminal);

        this.terminal.domElement.addEventListener('signal', function (e) {
            _this.getSignalState(e);
        }, false);

        // LOGGER
        this.loggerElement = document.querySelector('#audio-logger-data');

    }

    getSignalState(e) {
        console.log("SIGNAL CHANGE", e);

        var _this = this;

          if ( e.params.isOn ) {
            
            _this.micStream = new MicrophoneStream();

            getUserMedia({
                    video: false,
                    audio: true
                })
                .then(function(stream) {
                    _this.micStream.setStream(stream);
                }).catch(function(error) {
                    console.log(error);
                });

            // get Buffers (Essentially a Uint8Array DataView of the same Float32 values)
            _this.micStream.on('data', function(chunk) {
                // Optionally convert the Buffer back into a Float32Array
                // (This actually just creates a new DataView - the underlying audio data is not copied or modified.)
                var raw = MicrophoneStream.toRaw(chunk)
                //console.log(raw);

                // fft
                var phasors = fft(raw);

                var frequencies = fftUtil.fftFreq(phasors, 8000), // Sample rate and coef is just used for length, and frequency step
                    magnitudes = fftUtil.fftMag(phasors);

                _this.audioData = frequencies.map(function(f, ix) {
                    return {
                        frequency: f,
                        magnitude: magnitudes[ix]
                    };
                });

                // note: if you set options.objectMode=true, the `data` event will output AudioBuffers instead of Buffers
            });

            // or pipe it to another stream
            // _this.micStream.pipe(/*...*/);

            // It also emits a format event with various details (frequency, channels, etc)
            // _this.micStream.on('format', function(format) {
            //     console.log(format);
            // });
          } else if ( e.params.isOff ) {
            _this.micStream.stop();
          }
    }

        getAverageFFT() {

        var _this = this;

        if ( this.micStream) {
            
            if (this.micStream.context.state == "running") {
                
                var mean = 0;

                if (this.audioData != null) {
                    // console.log(this.audioData);
                    for (var i = 0; i < this.audioData.length; i++) {
                        mean += parseInt(this.audioData[i].magnitude, 10) * this.fftSensitivity;
                    }

                    this.fftAverage = Math.clamp(mean / this.audioData.length, 0.0, 1.0);
                    // console.log(this.fftAverage);

                    this.fftColor = new THREE.Color(this.fftAverage, this.fftAverage, this.fftAverage);

                    this.loggerElement.textContent += this.fftAverage + "\n";
                    this.loggerElement.scrollTop = this.loggerElement.scrollHeight;

                }

                (function() {
                    Math.clamp = function(a, b, c) {
                        return Math.max(b, Math.min(c, a));
                    }
                })();

            } else if (this.micStream.context.state == "closed") {
                this.loggerElement.textContent = " ";
            }

        }

    }

}
export default Audio;