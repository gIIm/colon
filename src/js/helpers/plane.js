import 'three'
import TWEEN from 'tween.js'

class Plane {

    constructor(app, terminal, gui) {

        var _this = this;

        // APP
        this.app = app;

        // GUI
        this.gui = gui;
        // this.addToGui(this.gui);

        // GROUP
        this.group = new THREE.Group();
        this.group.name = 'planeGroup';

        this.app._scene.add(this.group);

        // MESH INDEX
        this.meshIndex = 0;

        // TWEEN
        this.easing = TWEEN.Easing.Cubic.InOut;
        this.easingSpeed = 2000;

        // TERMINAL
        this.terminal = terminal;

        this.logger = terminal.logger;

        // EVENT
        this.terminal.domElement.addEventListener('createPlane', function(e) {
            _this.addPlane(e.params);
        }, false);

        this.terminal.domElement.addEventListener('deletePlane', function(e) {
            _this.deletePlane(e.params);
        }, false);

        this.terminal.domElement.addEventListener('goto', function(e) {
            // _this.goToCamera(e.params);
            _this.goToPlane(e.params);
        }, false);

    }

    addPlane(params) {

        var _this = this;

        this.logger.createNewTyper();

        for (var i = 0; i < params.num; i++) {

            if (params.num > 1) {

                params.posX = Math.random() * 600 - 300;
                params.posY = Math.random() * 600 - 300;
                params.posZ = Math.random() * -500;

                // params.rotX = Math.random() * Math.PI * 2;
                // params.rotY = Math.random() * Math.PI * 2;
                // params.rotZ = Math.random() * Math.PI * 2;
                params.rotX = Math.random() * 0.05;
                params.rotY = Math.random() * 0.05;
                params.rotZ = Math.random() * 0.05;

            }

            var geometry = new THREE.PlaneBufferGeometry(params.width, params.height, params.segments, params.segments);

            var material;

            var mesh;

            // FACE
            if (params.mode == "face" || params.mode == "faces") {

                material = new THREE.MeshPhongMaterial({
                    color: params.color,
                    side: THREE.DoubleSide
                });

            }

            // WIREFRAME
            if (params.mode == "wireframe") {

                material = new THREE.MeshBasicMaterial({
                    color: params.color,
                    wireframe: true
                });

            }

            // POINTS
            if (params.mode == "point" || params.mode == "points") {

                material = new THREE.PointsMaterial({
                    color: params.color
                });

                mesh = new THREE.Points(geometry, material);

            } else {

                mesh = new THREE.Mesh(geometry, material);

            }

            mesh.name = "plane::" + this.meshIndex;

            mesh.position.set(params.posX, params.posY, params.posZ);

            mesh.rotation.set(params.rotX * 40, params.rotY * 40, params.rotZ * 40);
            mesh.rotationSpeed = new THREE.Vector3(params.rotX, params.rotY, params.rotZ);

            //mesh.scale.set(1, 1, 1);

            this.group.add(mesh);

            this.meshIndex++;

            this.logger.typer.line(mesh.name, 1);
            this.logger.typer.line("::::::::::::::::::::::::::::::::::::::::", 2);
            this.logger.typer.line(mesh.name + "::pos: " + JSON.stringify(mesh.position), 1);
            this.logger.typer.line();
            this.logger.typer.line(mesh.name + "::rot: " + JSON.stringify(mesh.rotation), 1);
            this.logger.typer.line();
            this.logger.typer.line(mesh.name + "::width: " + params.width, 1);
            this.logger.typer.line();
            this.logger.typer.line(mesh.name + "::color: " + params.color, 1);
            this.logger.typer.line("::::::::::::::::::::::::::::::::::::::::", 2);

        }

    }

    addToGui(gui) {

        // gui.add(this.params, 'usePostProcessing');

        return gui;
    }

    update() {

        for (var i = 0; i < this.group.children.length; i++) {

            if (this.group.children[i]) {
                //this.group.children[i].rotation.x += this.group.children[i].rotationSpeed.x + ((i - this.group.children.length) * 0.00001);
                this.group.children[i].rotation.y += this.group.children[i].rotationSpeed.y + ((i - this.group.children.length) * 0.00001);
                // this.group.children[i].rotation.z += this.group.children[i].rotationSpeed.z + ((i - this.group.children.length) * 0.00001);
            }
        }

    }

    goToCamera(params) {

        var _this = this;

        if (params.type == "pos") {

            var vec = params.pos;

            if (vec != undefined) {

                var from = {
                    x: this.app._camera.position.x,
                    y: this.app._camera.position.y,
                    z: this.app._camera.position.z,
                };

                var to = {
                    x: vec.x,
                    y: vec.y,
                    z: vec.z,
                };

                var speed = params.speed;

                var tweenCamera = new TWEEN.Tween(from)
                    .to(to, speed)
                    .easing(_this.easing)
                    .onUpdate(function() {

                        _this.app._camera.position.set(this.x, this.y, this.z);
                        _this.app._camera.lookAt(new THREE.Vector3(0, 0, 0));

                    })
                    .onComplete(function() {

                        _this.app._camera.position.set(to.x, to.y, to.z);
                        _this.app._camera.lookAt(new THREE.Vector3(0, 0, 0));

                    })
                    .start();
            }
        }

    }

    goToPlane(params) {

        var _this = this;

        if (params.type == "plane") {

            var obj = this.app._scene.getObjectByName(params.id);

            if (obj != undefined) {

                var from = {
                    x: this.app._camera.position.x,
                    y: this.app._camera.position.y,
                    z: this.app._camera.position.z,
                };

                var to = {
                    x: obj.position.x,
                    y: obj.position.y,
                    z: obj.position.z + 150,
                };

                var speed = params.speed;

                var tweenCamera = new TWEEN.Tween(from)
                    .to(to, speed)
                    .easing(_this.easing)
                    .onUpdate(function() {

                        _this.app._camera.position.set(this.x, this.y, this.z);
                        _this.app._camera.lookAt(new THREE.Vector3(0, 0, 0));

                    })
                    .onComplete(function() {

                        _this.app._camera.position.set(to.x, to.y, to.z);
                        _this.app._camera.lookAt(new THREE.Vector3(0, 0, 0));

                    })
                    .start();
            }
        }

    }

    deletePlane(params) {

        if (!this.group.children.length) {

            this.logger.createNewTyper();

            this.logger.typer.line("<span style='background-color:" + this.logger.typerColorError + "'>::NO PLANE TO DELETE::</span>")



            return;
        }

        if (params.num == "all" || params.num == "ALL" || params.num > 1) {

            var num;

            if (params.num == "all" || params.num == "ALL") {
                num = this.group.children.length;
            } else {
                num = params.num;
            }

            if (num <= this.group.children.length) {

                num = this.group.children.length - num;

                for (var i = this.group.children.length - 1; i >= num; i--) {

                    if (this.group.children[i]) {

                        this.group.remove(this.group.children[i]);
                        // if (this.group.children[i].geometry !== undefined) this.group.children[i].geometry.dispose();
                        // if (this.group.children[i].material !== undefined) this.group.children[i].material.dispose();
                        // if (this.group.children[i] !== undefined) this.group.children[i] = undefined;
                    }

                }

                this.meshIndex = 0;

                this.logger.createNewTyper();

                this.logger.typer.line("<span style='background-color:" + this.logger.typerColorConfirm + "'>::PLANES REMAINING:: " + this.group.children.length + "</span>")

                return;

            }

        } else {

            params.id = 0;

            if (this.group.children[params.id]) {

                this.group.remove(this.group.children[params.id]);
                //             this.group.children[params.id].geometry.dispose();
                // this.group.children[params.id].material.dispose();
                // this.group.children[params.id] = undefined;

                this.logger.createNewTyper();

                this.logger.typer.line("<span style='background-color:" + this.logger.typerColorConfirm + "'>::PLANES REMAINING:: " + this.group.children.length + "</span>")
                return;

            }
        }



    }

}
export default Plane;