# COLON

Live Coding Virtual Environment for visuals



## Installation

```bash
npm install
npm run dev
```

Open http://localhost:8080



## Build

```bash
npm run build
```



## Commands



### exit

quit prompt




### re

return to previous prompt input




### follow

follow log




### release

release log




### signal

enable/disable audio signal

#### parameters

##### state

```c
on || true
off || false
```



### flush

empty logs



### debug

switch debug mode, display camera helper

#### parameters

##### state

```c
on || true
off || false
```



### ascii

switch ascii mode

#### parameters

##### state

```c
on || true
off || false
```



### create

create artefact instance

#### parameters

##### box

create box artefact

###### mode

```c
point || wireframe || face
```

###### num

```c
int
```

###### size

```c
int || float
```

###### pos

```c
x: int || float
y: int || float
z: int || float
```

###### rot

```c
x: int || float
y: int || float
z: int || float
```

###### color

```c
hex || rgb
```



##### :sphere

create sphere artefact

###### mode

```c
point || wireframe || face
```

###### num

```c
int
```

###### radius

```c
int || float
```

###### pos

```c
x: int || float
y: int || float
z: int || float
```

###### rot

```c
x: int || float
y: int || float
z: int || float
```

###### color

```c
hex || rgb
```



##### ::plane

create plane artefact

###### mode

```c
point || wireframe || face
```

###### num

```c
int
```

###### width

```c
int || float
```

###### height

```c
int || float
```

###### segments

```c
int
```

###### pos

```c
x: int || float
y: int || float
z: int || float
```

###### rot

```c
x: int || float
y: int || float
z: int || float
```

###### color

```c
hex || rgb
```



### delete

delete artefact instance

#### parameters

##### box

delete box artefact

###### num

```c
int || float
```

###### id

```c
int
```



##### :sphere

delete sphere artefact

###### num

```c
int || float
```

###### id

```c
int
```



##### ::plane

delete plane artefact

###### num

```c
int || float
```

###### id

```c
int
```



### goto

go to selected artefact position

#### parameters

##### artefact

go to selected artefact position

###### type

```c
box || sphere
```

###### id

```c
int
```

###### speed

```c
int || float
```



##### pos

go to selected position

###### vec3

```c
(x, y z)
```

###### speed

```c
int || float
```

